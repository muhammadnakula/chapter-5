const path    = require('path');
const express = require('express');
const router  = express.Router();

router.get('/', (req, res, next) => {
   res.sendFile(path.join(__dirname, '../challenge3/home.html'));
});
// router.get('/', (req, res, next) => {
//     res.sendFile(path.join(__dirname, '../challenge3/home.css'));
// });
router.get('/home', (req, res, next) => {
    res.sendFile(path.join(__dirname, '../challenge3/home.html'));
});
router.get('/games', (req, res, next) => {
    res.sendFile(path.join(__dirname, '../challenge3/games.html'));
});
router.get('/features', (req, res, next) => {
    res.sendFile(path.join(__dirname, '../challenge3/features.html'));
});
router.get('/scores', (req, res, next) => {
    res.sendFile(path.join(__dirname, '../challenge3/scores.html'));
});
router.get('/system', (req, res, next) => {
    res.sendFile(path.join(__dirname, '../challenge3/system.html'));
});
router.get('/jankenpon', (req, res, next) => {
    res.sendFile(path.join(__dirname, '../challenge4/index.html'));
});

module.exports = router;

// ==== MAAF SAYA BELUM TAU CARA MENGGABUNGKAN FILE CSS DENGAN HTML DI EXPRESS.JS